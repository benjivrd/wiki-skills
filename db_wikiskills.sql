-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Hôte : db5001143085.hosting-data.io
-- Généré le : sam. 07 nov. 2020 à 15:29
-- Version du serveur :  5.7.30-log
-- Version de PHP : 7.0.33-0+deb9u10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `db_wikiskills`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_publication` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id`, `author_id`, `category_id`, `title`, `content`, `image`, `date_publication`) VALUES
(39, 1, 4, 'C\'est quoi la POO', 'Vous avez des explication simple ? ', 'https://mdbootstrap.com/img/Photos/Others/images/4.jpg', '2020-11-06 12:19:57'),
(40, 15, 4, 'Symfony', 'Symfony est un ensemble de composants PHP ainsi qu\'un framework MVC libre écrit en PHP.', 'https://mdbootstrap.com/img/Photos/Others/images/11.jpg', '2020-11-06 12:20:53'),
(43, 1, 1, 'Framework React', 'Grâce à React, il est facile de créer des interfaces utilisateurs interactives. Définissez des vues simples pour chaque état de votre application, et lorsque vos données changeront, React mettra à jour, de façon optimale, juste les composants qui en auront besoin.', 'https://mdbootstrap.com/img/Photos/Others/images/9.jpg', '2020-11-06 12:32:26'),
(44, 16, 1, 'Les bases de JavaScript', 'JavaScript est un langage de programmation qui ajoute de l\'interactivité à votre site web (par exemple : jeux, réponses quand on clique sur un bouton ou des données entrées dans des formulaires, composition dynamique, animations). Cet article vous aide à débuter dans ce langage passionnant et vous donne une idée de ses possibilités.\r\n\r\nQu\'est le JavaScript, réellement ?\r\nJavaScript (« JS » en abrégé) est un langage de programmation dynamique complet qui, appliqué à un document HTML, peut fournir une interactivité dynamique sur les sites Web. Il a été inventé par Brendan Eich, co-fondateur du projet Mozilla, de la Mozilla Foundation et de la Mozilla Corporation.', 'https://mdbootstrap.com/img/Photos/Others/images/12.jpg', '2020-11-06 12:44:38'),
(45, 1, 1, 'Modif : Les bases du HTML', 'HyperText Markup Language (HTML) est le code utilisé pour structurer une page web et son contenu. Par exemple, le contenu de votre page pourra être structuré en un ensemble de paragraphes, une liste à puces ou avec des images et des tableaux de données. Comme le suggère le titre, cet article vous fournit les bases de compréhension du HTML et de ses fonctions.', 'https://mdbootstrap.com/img/Photos/Others/images/7.jpg', '2020-11-06 14:05:38'),
(50, 25, 1, 'Article', 'Je test votre site', 'https://mdbootstrap.com/img/Photos/Others/images/3.jpg', '2020-11-06 19:53:56'),
(51, 1, 4, 'Les avancées technologiques en PHP', 'Quelles sont les nouvelles avancées technologiques en termes de programmation', 'https://mdbootstrap.com/img/Photos/Others/images/9.jpg', '2020-11-07 09:38:36');

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Javascript'),
(2, 'HTML'),
(3, 'CSS'),
(4, 'PHP');

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_publication` datetime NOT NULL,
  `article_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `author_id`, `content`, `date_publication`, `article_id`) VALUES
(3, 1, 'La poo c\'est : Programmation Orienté Objet :)', '2020-11-06 14:03:30', 39),
(5, 18, 'Moi je préfère le PHP perso -_- ', '2020-11-06 14:17:01', 45),
(9, 19, 'wsh mais c\'est mon article ça', '2020-11-06 14:30:43', 45),
(16, 1, 'Perso je trouve que react c\'est un des meilleur framework JS', '2020-11-06 15:35:40', 43),
(19, 1, 'non c\'est vrai ?', '2020-11-06 16:54:28', 45),
(21, 25, 'Je teste', '2020-11-06 19:54:24', 50),
(22, 1, 'Alors le site ? ', '2020-11-07 09:37:27', 50);

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20201001142532', '2020-11-03 10:57:38', 651);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pseudo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_creation` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `pseudo`, `image`, `roles`, `password`, `date_creation`) VALUES
(1, 'ben@ben.fr', 'benji', 'https://mdbootstrap.com/img/Photos/Avatars/avatar-8.jpg', '[]', '$argon2id$v=19$m=65536,t=4,p=1$T3V0dUhSMUJVU0NxcjlZUw$zJ+1z09vZgm4kghDCU1jD6ciVQyEgcwpFjExKDhdCZI', '2020-11-06 12:17:05'),
(15, 'aubin.mahot@gmail.com', 'Swee', 'https://mdbootstrap.com/img/Photos/Avatars/avatar-7.jpg', '[]', '$argon2id$v=19$m=65536,t=4,p=1$63FBbHm9KKtaaQV9zoYQkw$buWJ+Fz9QftagZIEagVgOVWk1I4VU0rTsR1yhi8p+GI', '2020-11-06 12:18:41'),
(16, 'asav@gmail.com', 'Riwo', 'https://mdbootstrap.com/img/Photos/Avatars/avatar-5.jpg', '[]', '$argon2id$v=19$m=65536,t=4,p=1$xN2U133d+pj8UqdmCQEoNQ$CT4hNmrLtXBL7fJSSYK5F1FSkxyAqrfVGO4bAFzkkEw', '2020-11-06 12:20:49'),
(17, 'failederror@gmail.com', 'InspecteurGadget', 'https://mdbootstrap.com/img/Photos/Avatars/avatar-0.jpg', '[]', '$argon2id$v=19$m=65536,t=4,p=1$ioY0/gaL0KhaHpryKCfpdQ$wFY+N0ee91SpEqViCNXpRHpKuvjmYjct4AIyOBNzkfs', '2020-11-06 12:22:04'),
(18, 'benji@gmail.fr', 'g4-ben', 'https://mdbootstrap.com/img/Photos/Avatars/avatar-1.jpg', '[]', '$argon2id$v=19$m=65536,t=4,p=1$1gTjnU6R3UU5a7JD7oQUKQ$HxCqrkXq5oJH4i7pejPs5fAvN95jS3ipStnp6bAp9so', '2020-11-06 14:16:01'),
(19, 'jean-michel-ouelbeck@caramail.eu', 'jean-michel', 'https://mdbootstrap.com/img/Photos/Avatars/avatar-8.jpg', '[]', '$argon2id$v=19$m=65536,t=4,p=1$cipzGPhUeuMYFM59oEMLrg$clPa6FqzVJQO03cNwVfP+BQiBGNyxRH6RQxnHfghojY', '2020-11-06 14:29:00'),
(20, 'institug4@gmail.com', 'g4-officiel', 'https://mdbootstrap.com/img/Photos/Avatars/avatar-9.jpg', '[]', '$argon2id$v=19$m=65536,t=4,p=1$VCWrHezrQqAScjbwQxuXsw$nlHJJ66rHiJzJh5P0c5JIJF+9tf4e3XIWQvyd8bIj58', '2020-11-06 14:46:29'),
(21, 'tomurosa@mailinator.com', 'Cumque voluptatem qu', 'https://mdbootstrap.com/img/Photos/Avatars/avatar-8.jpg', '[]', '$argon2id$v=19$m=65536,t=4,p=1$LFZd984jLnVr5BgNLklUqw$yaXwl2OblIFILrYM0hHib9PhmzWMCgrdepJ53uxw3sA', '2020-11-06 14:54:42'),
(22, 'vaqes@mailinator.com', 'Quia nulla est eu a', 'https://mdbootstrap.com/img/Photos/Avatars/avatar-7.jpg', '[]', '$argon2id$v=19$m=65536,t=4,p=1$EMGa+CORLPCRDEqnVp2c4g$kKzINP7lVX+eYrRFLbIfYr78FcpSrZLdn8VgQojaBpM', '2020-11-06 14:58:00'),
(23, 'xaruby@mailinator.com', 'Illum est dolor vel', 'https://mdbootstrap.com/img/Photos/Avatars/avatar-3.jpg', '[]', '$argon2id$v=19$m=65536,t=4,p=1$QwkV5WPAN/mXsI1gOqpQ1Q$b5GlaIj3q9N8RuowGtBCVTqkoHnZa1BuHGpFCeiAE08', '2020-11-06 15:08:30'),
(24, 'g4@g4.fr', 'testg4', 'https://mdbootstrap.com/img/Photos/Avatars/avatar-5.jpg', '[]', '$argon2id$v=19$m=65536,t=4,p=1$md+z9FhKX3TjQ0oEwbXwwQ$vAumBHF78PfwIIk72QIKi3KVVEvq4Grh6mI/vhUpdiA', '2020-11-06 15:47:20'),
(25, 'bonbon@hotmail.fr', 'bonbon', 'https://mdbootstrap.com/img/Photos/Avatars/avatar-1.jpg', '[]', '$argon2id$v=19$m=65536,t=4,p=1$voKXGq72ULI1diAvLMeJ0g$omWd6dWX1NQCRYx1Ssp+mlxao52tHlH4fzNc40QQmRY', '2020-11-06 19:53:09');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_23A0E66F675F31B` (`author_id`),
  ADD KEY `IDX_23A0E6612469DE2` (`category_id`);

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author_id` (`author_id`),
  ADD KEY `article_id` (`article_id`);

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `FK_23A0E6612469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_23A0E66F675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
