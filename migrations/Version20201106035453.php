<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201106035453 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //$this->addSql('ALTER TABLE comment ADD article_id INT NOT NULL');
        $this->addSql("ALTER TABLE comment ADD CONSTRAINT `fk.comment.article.id` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment DROP article_id');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY article_id');
    }
}
