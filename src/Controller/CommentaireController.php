<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Comment;
use App\Entity\User;

class CommentaireController extends AbstractController
{
    /**
     * @Route("/commentaire", name="commentaire")
     */
    public function index()
    {
        return $this->render('commentaire/index.html.twig', [
            'controller_name' => 'CommentaireController',
        ]);
    }

    /**
     * @Route("/comment/add", name="add_comment")
     */
    public function add(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
         $comment = new Comment();
         $em = $this->getDoctrine()->getManager();
         $nbcontent = intval(strlen($request->get('content')));
         if ($nbcontent > 2) {
            $author = $em->getRepository(User::class)->find($this->getuser()->getId());
            $comment->setContent($request->get('content'));
            $comment->setArticleId($request->get('article_id'));
            $comment->setDatePublication((new \DateTime('now')));
            $author->addAuthorCom($comment);
            $this->addFlash('success', 'Votre commentaire à bien été crée');
            $em->persist($comment);
            $em->flush();

            $referer = $request->headers->get('referer');
            return $this->redirect($referer);
         }
         else{
            $this->addFlash('danger', 'Votre commentaire doit contenir au moins 2 caractères ');
            $referer = $request->headers->get('referer');
            return $this->redirect($referer);
         }
        
             
    }

        /**
     * @Route("/comment/delete/{id}", name="delete_comment")
     */
    public function delete(Request $request){
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

         
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository(Comment::class)->findById($request->get('id'));
        $user = $em->getRepository(User::class)->findById($this->getUser()->getId());

            $em->getRepository(Comment::class)->remove($request->get('id'));
            $em->flush();
            $this->addFlash('info','Votre commentaire à bien été supprimer');

            $referer = $request->headers->get('referer');
            return $this->redirect($referer);
      

       
       
    }
}
