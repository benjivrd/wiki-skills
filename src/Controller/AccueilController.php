<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\User;

class AccueilController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index()
    {    $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository(Article::class)->findAllArticle();
        $userImg = $em->getRepository(User::class)->findImg();
        $categories = $em->getRepository(Category::class)->findAll();
        //dd($userImg);
        return $this->render('accueil/index.html.twig',[
            'articles' => $articles, 'categories' => $categories, 'users' => $userImg
        ]);
    }
}
