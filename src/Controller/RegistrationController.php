<?php

namespace App\Controller;

use App\Entity\User;
use App\Security\LoginAuthenticator;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, LoginAuthenticator $authenticator): Response
    {


        if ($this->getUser()) {
           return $this->redirectToRoute('show');
        }

        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                ),
            $user->setDateCreation((new \DateTime('now'))),
            $user->setImage("https://mdbootstrap.com/img/Photos/Avatars/avatar-".strval(random_int(0,10)).".jpg")
            );

            $entityManager = $this->getDoctrine()->getManager();
            $this->addFlash('success', 'Votre compte à bien été crée');
            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email
            

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }


        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
            'errors' => $form->getErrors()
        ]);
    }
}
