<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article; 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\NewArticleFormType;
use App\Form\EditArticleFormType;
use App\Repository\UserRepository;
use App\Entity\User;
use App\Entity\Category;
use App\Entity\Comment;

class ArticleController extends AbstractController
{

    /**
     * @Route("/articles", name="show")
     */
    public function show(){
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository(Article::class)->findAllByAuthor($this->getUser()->getId());
       // dd($articles);
        return $this->render('articles/show.html.twig',[
            'articles' => $articles
        ]);
    }
    /**
     * @Route("/article/new", name="new")
     */
    public function new(Request $request){
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
         $article = new Article();
         $form = $this->createForm(NewArticleFormType::class, $article);
         $em = $this->getDoctrine()->getManager();
         $catergory = $em->getRepository(Category::class)->findAll();
         $form->handleRequest($request);
         if ($form->isSubmitted() && $form->isValid()) {
            $author = $em->getRepository(User::class)->find($this->getuser()->getId());
            $data = $request->get('new_article_form');
            $author->addAuthorArt($article);
            $article->setTitle($data['title']);
            $article->setContent($data['content']);
            $article->setImage('https://mdbootstrap.com/img/Photos/Others/images/'.strval(random_int(0, 20)).'.jpg');
            $article->setDatePublication((new \DateTime('now')));
            $this->addFlash('success', 'Votre article à bien été crée');
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('show');
             
         }
 
         return $this->render('articles/new.html.twig', [
             'newArticleForm' => $form->createView(),
             'categories' => $catergory
         ]);

       
    }

      /**
     * @Route("/article/delete/{id}", name="delete")
     */
    public function delete(Request $request){
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

         
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->findById($request->get('id'));

        if ($article[0]->getAuthor()->getId() == $this->getUser()->getId()) {
            $em->getRepository(Article::class)->remove($request->get('id'));
            $em->flush();
            $this->addFlash('info','Votre article à bien été supprimer');

            return $this->redirectToRoute('show');
        }
        else{
            $this->addFlash('danger','Connexion refusé');
            return $this->redirectToRoute('show');
        }

       
       
    }
       /**
     * @Route("/article/show/{id}", name="show_article")
     */
    public function showDetail(Request $request){
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->findById($request->get('id'));
        $comments = $em->getRepository(Comment::class)->findByArticle($request->get('id'));
     
        return $this->render('articles/show_detail.html.twig',[
            'article' => $article[0],
            'comments' => $comments
        ]);

       
       
    }
    /**
     * @Route("/article/edit/{id}", name="edit")
     */
    public function edit(Request $request){
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

         
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->findById($request->get('id'));

        if ($article) {
            if ($article[0]->getAuthor()->getId() == $this->getUser()->getId()){
                $catergory = $em->getRepository(Category::class)->findAll();
                $form = $this->createForm(EditArticleFormType::class, $article, ["data_class" => null]);
                $form->get('title')->setData($article[0]->getTitle());
                $form->get('content')->setData($article[0]->getContent());
                $form->get('image')->setData($article[0]->getImage());
        
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $author = $em->getRepository(User::class)->find($this->getuser()->getId());
                    $data = $request->get('edit_article_form');
                    $article[0]->setTitle($data['title']);
                    $article[0]->setContent($data['content']);
                    $article[0]->setImage($data['image']);
                    $article[0]->setDatePublication((new \DateTime('now')));
                    $article[0]->setCategory($catergory[$request->get('category') - 1]);
                    $this->addFlash('success', 'Votre article à bien été mis à jour');
                    $em->persist($article[0]);
                    $em->flush();
        
                    return $this->redirectToRoute('show');
                    
                }
            }else{
                $this->addFlash('danger', 'Connexion refusé');
                return $this->redirectToRoute('show');
            }
        }else{
            $this->addFlash('danger', 'Article non trouvé');
            return $this->redirectToRoute('show');
        }
        


        return $this->render('articles/edit.html.twig', [
            'editArticleForm' => $form->createView(),
            'categories' => $catergory
        ]);
       
    }

}