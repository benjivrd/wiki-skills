<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email as ConstraintsEmail;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'empty_data' => '',
                'constraints' => [
                    new ConstraintsEmail([
                        'message' => 'Veuillez saisir une adresse mail valide',
                    ])
                    ],
                'label'=>"Ton email"
            ])
            ->add('pseudo',TextType::class,[
                'label' => "Ton pseudo",
                'constraints' => [
                    new NotBlank(['message' => 'Votre pseudo ne doit pas être vide']),
                    new Length([
                        'min' => 4,
                        'minMessage' => ' Le pseudo dois contenir au moins {{ limit }} caracteres',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ])
                ]
            ])
            ->add('cdn', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Tu dois accepte les conditions generales d utilisation.',
                    ]),
                ],
                'label'=>" Accepter les conditions générales d'utilisation"
            ])
            ->add('password', RepeatedType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'type' => PasswordType::class,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Votre mot de passe ne dois pas être vide',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => ' Le mot de passe dois contenir au moins {{ limit }} caracteres',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
                'invalid_message' => "Les mots de passe ne correspondent pas",
                'first_options' => ['label' => 'Votre mot de passe'],
                'second_options' => ['label' => 'Retaper votre mot de passe']

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
