<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    public function findAllByAuthor($id)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.author = :val')
            ->setParameter('val', $id)
            ->orderBy('a.id', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    public function findAllArticle(): array
    {
        $conn = $this->getEntityManager()->getConnection();
    
        $sql = '
            SELECT a.id,pseudo,c.name,a.content,a.image,a.date_publication,a.title FROM article a
            INNER JOIN user u on u.id = a.author_id
            INNER JOIN category c on c.id = a.category_id
            ORDER BY a.id DESC
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    
        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAllAssociative();
    }

    public function remove($id){
        return $this->createQueryBuilder('a')
                ->delete(Article::class, 'a')
                ->where('a.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->execute(); 
        ;
    
    }


    

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}